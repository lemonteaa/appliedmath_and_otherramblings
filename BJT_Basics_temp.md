﻿# BJT Basics

## Device Model

The $\alpha$ parameter is usually slightly less than 1 and indicate the portion of the emitter current contributed by the collector (the rest is from the base). The $\beta$ parameter is a large positive number and represents the amplification from the base to the collector. More formally,

$$ \alpha = i_C / i_E, \beta = i_C / i_B \\
\rightarrow \alpha = \frac{\beta i_B}{i_B + i_C} = \frac{\beta}{1+\beta} \\
\rightarrow \beta = \frac{\alpha}{1 - \alpha}$$

### Ebers-Moll

Image lost, archive available at https://web.archive.org/web/20190903094420im_/http://people.seas.harvard.edu/~jones/es154/lectures/lecture_3/bjt_models/ebers_moll/ebers_moll.gif
(Please don't bombard it with requests)

(The dependent current sources' direction is flipped)

From the diode equation we have $i_{DC} = i_{SC} (e^{V_{BC}/V_T} - 1)$ and $i_{DE} = i_{SE} (e^{V_{BE}/V_T} - 1)$.

Also,
$$
\begin{array}{rl}
i_E &= i_{DE} - \alpha_R i_{DC} \\
i_C &= -i_{DC} + \alpha_F i_{DE} \\
i_B &= i_E - i_C \\
&= (1 - \alpha_F) i_{DE} + (1- \alpha_R) i_{DC}
\end{array}
$$

Note that $\alpha_F i_{SE} = \alpha_R i_{SC} = I_S$, hence
$$
\begin{array}{rl}
i_E &= \frac{I_S}{\alpha_F} (e^{V_{BE}/V_T} - 1) - I_S (e^{V_{BC}/V_T} - 1) \\
i_C &= I_S (e^{V_{BE}/V_T} - 1) - \frac{I_S}{\alpha_R} (e^{V_{BC}/V_T} - 1) \\
i_B &= I_S \left[ \frac{1}{\beta_F} (e^{V_{BE}/V_T} - 1) + \frac{1}{\beta_R} (e^{V_{BC}/V_T} - 1) \right]
\end{array}
$$

## Load Line
![Load Line](https://upload.wikimedia.org/wikipedia/commons/2/27/BJT_CE_load_line.svg)

(Image from [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:BJT_CE_load_line.svg) by [Wtshymanski](https://commons.wikimedia.org/wiki/User:Wtshymanski), licensed under [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en))

We would like to solve for $i_C$ given $i_B$ and $V_{CE}$.

First note that $V_{CE} = V_{CB} + V_{BE} = - V_{BC} + V_{BE}$. This gives us a nonlinear system of equations in $V_{BC}$ and $V_{BE}$, which can be visualised to help determine some qualitative behaviour (as we show in the next section). However it is possible to get an analytic solution by using a clever change of perspective.

Observe that the currents $i_C, i_B, i_E$ are expressed as linear combination of $I_{BC} = e^{V_{BC}/V_T} - 1$ and $I_{BE} = e^{V_{BE}/V_T} - 1$. So having the value of any two of them is enough to uniquely solve for the two variables:

$$
\left(\begin{matrix}
i_C \\ i_B \end{matrix}\right)
= I_S \left(\begin{matrix}
1 & -\frac{1}{\alpha_R} \\
\frac{1}{\beta_F} & \frac{1}{\beta_R}
\end{matrix}\right)
\left(\begin{matrix}
I_{BE} \\ I_{BC} \end{matrix}\right) \\
\rightarrow
I_S \left(\begin{matrix}
I_{BE} \\ I_{BC} \end{matrix}\right) =
\left( \frac{1}{\beta_R} + \frac{1}{\beta_F \alpha_R} \right)^{-1}
\left(\begin{matrix}
\frac{1}{\beta_R} & \frac{1}{\alpha_R} \\
-\frac{1}{\beta_F} & 1
\end{matrix}\right)
\left(\begin{matrix}
i_C \\ i_B \end{matrix}\right)
$$

The determinant part can be simplified: $\frac{1}{\beta_R} + \frac{1}{\beta_F \alpha_R} = \frac{1}{\beta_R} + \frac{1}{\beta_F} (\frac{1}{\beta_R} + 1) = (1 + \frac{1}{\beta_F})(1 + \frac{1}{\beta_R}) - 1 = (\alpha_F \alpha_R)^{-1} - 1 = K$.

Then we can express $V_{BE}$ and $V_{BC}$ in terms of $I_{BE}$ and $I_{BC}$:

$$
V_{BE} = V_T \ln ( 1 + I_{BE}) \\
V_{BC} = V_T \ln ( 1 + I_{BC})
$$

Substitute into the equation for $V_{CE}$:

$$
V_{CE} = V_{BE} - V_{BC} = V_T ( \ln (1 + I_{BE}) - \ln (1 + I_{BC}) ) \\
= V_T \left( \ln ( 1 + \frac{ \beta_R^{-1} i_C + \alpha_R^{-1} i_B }{I_S ((\alpha_F \alpha_R)^{-1} - 1)}) - \ln ( 1 + \frac{ i_B - \beta_F^{-1} i_C }{I_S ((\alpha_F \alpha_R)^{-1} - 1)}) \right) \\
 \rightarrow
e^{V_{CE}/V_T} = \frac{I_S K + \alpha_R^{-1} i_B + \beta_R^{-1} i_C }{I_S K + i_B - \beta_F^{-1} i_C}
$$

which is a fractional linear equation. Letting $E = e^{V_{CE}/V_T}$ temporarily,

$$
I_S K E + E i_B - \frac{E}{\beta_F} i_C = I_S K + \frac{1}{\alpha_R} i_B + \frac{1}{\beta_R} i_C \\
( \frac{E}{\beta_F} + \frac{1}{\beta_R} ) i_C - (E - \frac{1}{\alpha_R}) i_B = I_S K (E - 1) \\
i_C = \frac{I_S K (E - 1) + (E - 1/\alpha_R) i_B}{E/\beta_F + 1/\beta_R}
$$

Looking carefully at this solution, it is the separable sum of a constant part:
$$
\frac{I_S K (E - 1)}{E/\beta_F + 1/\beta_R}
$$
and a part that is directly proportional to $i_B$:
$$
\frac{E - 1/\alpha_R}{E/\beta_F + 1/\beta_R}
$$

Notice that they are both essentially logistic function up to an affine transform. Their "width" (whether they are squeezed horizontally) is fixed since the parameter inside the exponential is always scaled by $1/V_T$. Thus it suffices to find the point of half-life, the minimum and maximum (attained at $V_{CE} \rightarrow \pm \infty$, for which $E \rightarrow 0$ or $+\infty$).

The results are shown in the table below:

Part | Minimum | Maximum | Half-life at
-----|------------|-------------|---------
Linear/Active ($i_B$ part) | $-(\beta_R + 1)$ | $\beta_F$ | $V_T \ln (\frac{\beta_F}{\beta_R})$
Cut-off (constant part) | $-I_S K \beta_R$ | $I_S K \beta_F$ | $2V_T \ln (\frac{\beta_F}{\beta_R})$

$$
\frac{E - 1/\alpha_R}{E/\beta_F + 1/\beta_R} = 0.5(\beta_F - \beta_R - 1) \\
E - 1/\alpha_R = 0.5 E (1 - \frac{\beta_R + 1}{\beta_F}) + 0.5(\frac{\beta_F -1}{\beta_R}  - 1) \\
0.5(1 + \frac{\beta_R + 1}{\beta_F}) E = 0.5 (1 + \frac{\beta_F - 1}{\beta_R} + \frac{2}{\beta_R}) \\
(\frac{\beta_F + \beta_R + 1}{\beta_F}) E = \frac{\beta_R + \beta_F + 1}{\beta_R} \\
E = \frac{\beta_F}{\beta_R} \rightarrow V_{CE} = V_T \ln (\frac{\beta_F}{\beta_R})
$$

$$
\frac{I_S K (E - 1)}{E/\beta_F + 1/\beta_R} = I_S K (\beta_F - \beta_R) \\
E - 1 = (\beta_F - \beta_R)(E/\beta_F + 1/\beta_R) \\
\frac{\beta_R}{\beta_F} E = 1 + \frac{\beta_F - \beta_R}{\beta_R} \\
E = (\frac{\beta_F}{\beta_R})^2 \rightarrow V_{CE} = 2  V_T \ln ( \frac{\beta_F}{\beta_R})
$$

## Mode of Operation

For each of the conceptual diode, it is in forward mode when $V>0$, otherwise it's in reverse mode.

Collector Diode ($V_{BC}$) | Emitter Diode ($V_{BE}$) | Mode
---------------------------|--------------------------|--------
Reverse                    | Forward                  | Active
Forward                    | Forward                  | Saturation
Reverse                    | Reverse                  | Cut-off

Cut-off only occur when $i_B$ is sufficiently low. Suppose we exceeded that threshold (which will be determined later). Then Saturation begins once $V_{BC}$ drops below 0. We solve for the switching point by substutiting $V_{BC} = 0$ to the equations:

$$i_B = \frac{I_S}{\beta_F} ( e^{V_{BE}/V_T} - 1) \\
i_C = \beta_F i_B \\
V_{CE} = V_{BE}$$

Hence, the locus of the boundary is given in the load diagram as the curve $i_C = I_S (e^{V_{CE}/V_T} - 1)$.

As the voltage shrink to zero, we have $V_{BC} = V_{BE}$, which is a special case that we can solve:

$$
i_B = I_S \left[ \frac{1}{\beta_F} (e^{V_{BE}/V_T} - 1) + \frac{1}{\beta_R} (e^{V_{BC}/V_T} - 1) \right] \\
= I_S (\frac{1}{\beta_F} + \frac{1}{\beta_R}) (e^{V/V_T} - 1)
$$

and so

$$
i_C = I_S ( 1 - \frac{1}{\alpha_R}) (e^{V/V_T} - 1) = i_B \frac{1 - \frac{1}{\alpha_R}}{\frac{1}{\beta_F} + \frac{1}{\beta_R}}
$$

In the active region, we want to know the asymptote. In the limit, we ought to have $V_{BC} \rightarrow -\infty$ while $V_{BE}$ converge to a finite positive value. Indeed we solve again:

$$
i_B =  I_S \left[ \frac{1}{\beta_F} (e^{V_{BE}/V_T} - 1) - \frac{1}{\beta_R} \right] \\
\frac{i_B}{I_S} + \frac{1}{\beta_R} = \frac{1}{\beta_F} (e^{V_{BE}/V_T} - 1)
$$

Substitute into $i_C$:

$$
i_C = I_S (e^{V_{BE}/V_T} - 1) + \frac{I_S}{\alpha_R} \\
= I_S \left(  \beta_F (\frac{i_B}{I_S} + \frac{1}{\beta_R}) + \frac{1}{\alpha_R} \right) \\
= \beta_F i_B + I_S ( \frac{\beta_F}{\beta_R} + \frac{1}{\alpha_R} ) \\
= \beta_F i_B + I_S (1 + \frac{\beta_F + 1}{\beta_R})
$$

So there is a constant extra current on top of the linear amplification.

The above discussion assume that $V_{BE} > 0$ throughout. We solve for the value of $V_{BC}$ such that $V_{BE} = 0$:

$$
i_B = \frac{I_S}{\beta_R} ( e^{V_{BC}/V_T} - 1) \\
\rightarrow V_{CE} = -V_{BC} = -V_T \ln ( 1 + \frac{\beta_R i_B}{I_S} )
$$

This happens only if $V_{CE} >= 0$, only if $\ln ( 1 + \frac{\beta_R i_B}{I_S} ) <= 0$, or $i_B <= 0$.


### Unused material

Linearize the equations where we keep $i_B$ constant and varies $V_{CE}$ linearly:

$$
\begin{array}{rrl}
\frac{I_S}{\beta_F V_T} e^{V_{BE}/V_T} dV_{BE} +& \frac{I_S}{\beta_R V_T} e^{V_{BC}/V_T} dV_{BC} &= 0 \\
dV_{BE} -& dV_{BC} &= 1
\end{array}
$$

Solution is $dV_{BE} = \frac{ \beta_R^{-1} e^{V_{BC}/V_T}}{\beta_F^{-1} e^{V_{BE}/V_T} + \beta_R^{-1} e^{V_{BC}/V_T}}$, $dV_{BC} = \frac{- \beta_F^{-1} e^{V_{BE}/V_T}}{\beta_F^{-1} e^{V_{BE}/V_T} + \beta_R^{-1} e^{V_{BC}/V_T}}$. In fact the denominator can be simplified as $i_B / I_S + (\beta_F^{-1} + \beta_R^{-1})$ which is manifestedly a constant.

We can then use this to derive the rate of change of $i_C$:
$$di_C = \frac{I_S}{V_T} e^{V_{BE}/V_T} dV_{BE} - \frac{I_S}{\alpha_R V_T} e^{V_{BC}/V_T} dV_{BC} \\
= \frac{I_S}{V_T (i_B / I_S + (\beta_F^{-1} + \beta_R^{-1}))} \left[ \frac{e^{(V_{BE} + V_{BC})/V_T}}{\beta_R} + \frac{e^{(V_{BE} + V_{BC})/V_T}}{\alpha_R \beta_F}\right] \\
= \frac{I_S ( \alpha_F^{-1} \alpha_R^{-1} - 1)e^{(V_{BE} + V_{BC})/V_T}}{V_T(i_B / I_S + (\beta_F^{-1} + \beta_R^{-1}))} $$

### Defunct
![Basic Ebers Moll Model](http://people.seas.harvard.edu/~jones/es154/lectures/lecture_3/bjt_models/ebers_moll/ebers_moll.gif)


> Written with [StackEdit](https://stackedit.io/).
